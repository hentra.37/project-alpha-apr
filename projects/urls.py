from django.urls import path
from projects.views import (
    ProjectView,
    ProjectDetail,
    ProjectCreate,
)

urlpatterns = [
    path("", ProjectView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetail.as_view(), name="show_project"),
    path("create/", ProjectCreate.as_view(), name="create_project"),
]
