from django.urls import path
from tasks.views import TaskCreate, TaskList, TaskUpdate

urlpatterns = [
    path("create/", TaskCreate.as_view(), name="create_task"),
    path("mine/", TaskList.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdate.as_view(), name="complete_task"),
]
